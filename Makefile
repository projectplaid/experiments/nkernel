CFLAGS=-g -O0 -ffreestanding -mcpu=cortex-a72
NIMFLAGS=--os:any --mm:arc -d:useMalloc --cpu:arm64 --define:qemu_virt=1
QEMU_DEFAULT=-M virt,gic-version=3,virtualization=true,secure=true -cpu cortex-a72 -m 1G -smp 1 -semihosting -kernel out/kernel.elf \
	-serial mon:stdio -nographic -d cpu_reset,guest_errors,int

#OBJS = out/start.o \
#	out/vectors.o \
#	out/gic.o \

.PHONY: all clean run out/nkernel.elf

default: all

all: out/nkernel.elf

out:
	mkdir -p out

out/%.o: %.S
	aarch64-none-linux-gnu-gcc $(CFLAGS) -c -o out/$*.o $*.S

out/nkernel.elf: out $(OBJS)
	nim c $(NIMFLAGS) -o:out/nkernel.elf src/nkernel.nim

clean:
	rm -f out/* $(INCS)

out/nkernel.img: out/nkernel.elf
	aarch64-none-linux-gnu-objcopy -O binary out/nkernel.elf out/nkernel.img

run: out/nkernel.img
	qemu-system-aarch64 $(QEMU_DEFAULT) $(QEMU_ARGS)

debug: out/nkernel.img
	qemu-system-aarch64 $(QEMU_DEFAULT) -S -s $(QEMU_ARGS)
