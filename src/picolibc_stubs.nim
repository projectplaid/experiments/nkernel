# Copyright 2024 Robert Roland.

# this module is required for our picolibc
# wrapper functions, so make sure it is
# never marked as unused
{.used.}

var write_func*: proc(ch: char)

proc exit(): int {.exportc: "_exit".} =
    while true:
        continue

proc putc(c: char, f: pointer): cint {.exportc: "_putc".} =
    if write_func != nil:
        write_func(c)
    result = cast[cint](c)

proc getc(f: pointer): cint {.exportc: "_getc".} =
    result = 0

{.
  emit:
    """
#include <stdio.h>

static FILE __stdio = FDEV_SETUP_STREAM(_putc, _getc, NULL, _FDEV_SETUP_RW);
FILE *const stdin = &__stdio; __strong_reference(stdin, stdout); __strong_reference(stdin, stderr);
"""
.}
