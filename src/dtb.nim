# Copyright 2024 Robert Roland.

# TODO remove this
{.used.}

import std/endians
import std/strformat
import system

import mmio

const
    FDT_BEGIN_NODE = 0x00000001
    FDT_END_NODE = 0x00000002
    FDT_PROP = 0x00000003
    FDT_NOP = 0x00000004
    FDT_END = 0x00000009

    validMagic = 0xD00DFEED

type
    DeviceTree* {.packed.} = object
        Magic*: uint32
        TotalSize*: uint32
        OffsetDTStruct*: uint32
        OffsetDTStrings*: uint32
        OffsetMemResMap*: uint32
        Version*: uint32
        LastCompatibleVersion*: uint32
        BootCPUIDPhys*: uint32
        SizeDTStrings*: uint32
        SizeDTStruct*: uint32

    ReservationEntry* {.packed.} = object
        Address*: uint64
        Size*: uint64

    FDTProperty* {.packed.} = object
        Length*: uint32
        NameOffset*: uint32

    ParsedTreeProperty* = object
        Name*: string
        Value*: seq[byte]

    ParsedTree* = object
        Name*: string
        UnitAddress*: string
        Children*: seq[ref ParsedTree]
        Parent*: ptr ParsedTree
        Properties*: seq[ref ParsedTreeProperty]

    PropertyType* = enum
        ptStringList,
        ptString,
        ptUInt32,
        ptUInt64,
        ptPHandle,
        ptPropEncodedArray,
        ptEmpty,
        ptByteArray

    DTBParser* = object
        BaseAddress: uint32
        DTB: DeviceTree
        ParsedTree: ref ParsedTree

proc swapBytes(x: uint32): uint32 =
    swapEndian32(addr result, addr x)

proc verifyHeader(dtb: ref DTBParser): bool =
    var headerValue: uint32 = GET32(dtb.BaseAddress)

    let v =
        if Endianness.bigEndian == system.cpuEndian: headerValue
        else: swapBytes(headerValue)

    result = v == cast[uint32](validMagic)

proc parseHeader(dtb: ref DTBParser): DeviceTree = 
    return DeviceTree()

proc newParser*(baseAddress: uint32): ref DTBParser =
    var parser = new(DTBParser)

    parser.BaseAddress = baseAddress

    if not verifyHeader(parser):
        parser = nil

    result = parser 
