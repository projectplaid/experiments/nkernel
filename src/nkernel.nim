# Copyright 2024 Robert Roland.

import picolibc_stubs
import dtb

when defined(qemu_virt):
    const
        UART0_BASE = 0x9000000
        DTB_BASE = 0x40000000

    import bsp/qemu_virt/uart

when isMainModule:
    when defined(qemu_virt):
        UARTInit(UART0_BASE)
        write_func = proc(ch: char) =
            UARTPuts(UART0_BASE, ch)

    echo("Hello world from Nim!")
    echo("DTB Parser: ", newParser(DTB_BASE).repr)
