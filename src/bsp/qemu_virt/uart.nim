# Copyright 2024 Robert Roland.

# qemu-virt UART functions

import ../../mmio

const
    UART_DR = 0x00
    UART_FR = 0x018
    UART_IBRD = 0x024
    UART_FBRD = 0x028
    UART_LCRH = 0x02C
    UART_CR = 0x030
    UART_IMSC = 0x038
    UART_ICR = 0x044
    UART_DMACR = 0x048

    CLOCK_RATE = 24000000 # 24 mHz
    BAUD_RATE = 115200

type
    TUART = object
    PUART = ptr TUART

var UART0: PUART = nil

proc UARTInit*(base: uint64) =
    UART0 = cast[PUART](base)

    # Disable the UART to begin with
    PUT32(base + UART_CR, 0)

    # Clear all interrupts
    PUT32(base + UART_ICR, 0x7ff)

    # This is going to assume a clock rate of 24 mHz
    # divisor = frequency of UART clock / (16 * baud rate)
    var divisor = 4 * CLOCK_RATE div BAUD_RATE
    var fbaud = divisor and 0x3f
    var ibaud = (divisor shr 6) and 0xffff

    PUT32(base + UART_IBRD, cast[uint32](ibaud))
    PUT32(base + UART_FBRD, cast[uint32](fbaud))

    # 8n1, enable FIFO
    PUT32(base + UART_LCRH, (0x7 shl 4))

    # Enable RX and TX
    PUT32(base + UART_CR, 0x301)

proc UARTPuts*(base: uint64, ch: char) =
    while true:
        if (GET32(base + UART_FR) and 0x20) == 0:
            break

    PUT32(base + UART_DR, cast[uint32](ch))

proc UARTGet*(base: uint64): char =
    while true:
        if (GET32(base + UART_FR) and 0x10) == 0:
            break

    result = cast[char](GET32(base + UART_DR))

proc UARTFlush*(base: uint64) =
    PUT32(base + UART_LCRH, (1 shl 4))
