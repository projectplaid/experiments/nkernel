# Copyright 2024 Robert Roland.

import std/volatile

proc PUT32*(address: uint32, value: uint32) =
    var p = cast[ptr uint32](address)
    volatileStore(p, value)

proc PUT32*(address: uint64, value: uint32) =
    var p = cast[ptr uint64](address)
    volatileStore(p, value)

proc GET32*(address: uint32): uint32 =
    var p = cast[ptr uint32](address)
    result = volatileLoad(p)

proc GET32*(address: uint64): uint32 =
    var p = cast[ptr uint64](address)
    result = cast[uint32](volatileLoad(p))
