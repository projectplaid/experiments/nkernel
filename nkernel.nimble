# Package

version       = "0.1.0"
author        = "Robert Roland"
description   = "A new awesome nimble package"
license       = "MIT"
srcDir        = "src"
bin           = @["nkernel"]


# Dependencies

requires "nim >= 2.0.4"
